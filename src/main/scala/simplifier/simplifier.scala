package simplifier

import java.util.stream.Collectors

import AST._

// to implement
// avoid one huge match of cases
// take into account non-greedy strategies to resolve cases with power laws
object Simplifier {

  private val arithIntOp: Map[String, (Int, Int) => Int] = Map(("+", _ + _), ("-", _ - _), ("*", _ * _), ("/", _ / _), ("^", _ ^ _), ("%", _ % _))
  private val compareOps: Map[String, String] = Map(("<", ">="), (">", "<="), (">=", "<"), ("<=", ">"), ("==", "!="), ("!=", "=="))
  //private val logicalOp: Map[String, (Boolean, Boolean) => Tuple2[Boolean, Boolean]] =
  //  Map(("and", (a: Boolean, b: Boolean) => Tuple2(a && b, a || b)), ("or", (a: Boolean, b: Boolean) => Tuple2(a || b, a && b)))
  private val logicalOp: Map[String, (Boolean, Boolean) => Boolean] = Map(("and", _ && _), ("or", _ || _))

  implicit def logicalToConst(b: Boolean): LogicalConst = if (b) TrueConst() else FalseConst()

  def simplifyDivision(a: Node, b: Node): Node = {
    if (a == b) return IntNum(1)
    (a, b) match {
      case (IntNum(1), BinExpr("/", IntNum(1), right)) => return right
      case _ => BinExpr("/", a, b)
    }
    BinExpr("/", a, b)
  }

  def equalBinExps(a: BinExpr, b: BinExpr) = (a.op == b.op) &&
    ((a.right == b.right && a.left == b.left) ||
      (Set("+", "*", "and", "or", "==", "!=").contains(a.op) && a.right == b.left && a.left == b.right))

  def simplifyMultiplication(a: Node, b: Node) = (a, b) match {
    case (a: Node, IntNum(1)) => a
    case (IntNum(1), b: Node) => b
    case (a: Node, IntNum(0)) => IntNum(0)
    case (IntNum(0), b: Node) => IntNum(0)
    case (a: Node, IntNum(-1)) => Unary("-", a)
    case (IntNum(-1), b: Node) => Unary("-", b)
    case _ => BinExpr("*", a, b)
  }

  def simplifyBinExpr(binExpr: BinExpr): Node = binExpr match {
    case BinExpr(o, a: IntNum, b: IntNum) => IntNum(arithIntOp(o)(a.value, b.value))
    case BinExpr(o, a: LogicalConst, b: LogicalConst) => logicalOp(o)(a.value, b.value)
    case BinExpr(o, a: Node, b: TrueConst) => if (o == "and") a else b
    case BinExpr(o, a: Node, b: FalseConst) => if (o == "and") b else a
    case BinExpr(o, a: TrueConst, b: Node) => if (o == "and") b else a
    case BinExpr(o, a: FalseConst, b: Node) => if (o == "and") a else b
    case BinExpr("+", a: ElemList, b: ElemList) => ElemList(a.list ::: b.list)
    case BinExpr("and", a: BinExpr, b: BinExpr) if equalBinExps(a, b) => a
    case BinExpr("or", a: BinExpr, b: BinExpr) if equalBinExps(a, b) => a
    case BinExpr("-", BinExpr("+", a: Node, b: Node), c: Node) if a == c => b
    case BinExpr("+", BinExpr("-", a: Node, b: Node), c: Node) if b == c => a
    case BinExpr("+", IntNum(0), b: Node) => b
    case BinExpr("+", a: Node, IntNum(0)) => a
    case BinExpr("-", IntNum(0), b: Node) => Unary("-", b)
    case BinExpr("-", a: Node, IntNum(0)) => a
    case BinExpr("*", a: Node, b: Node) if simplifyMultiplication(a, b) != binExpr => simplifyMultiplication(a, b)
    case BinExpr("/", IntNum(0), b: Node) => IntNum(0)
    case BinExpr("+", a: Unary, b: Node) => simplifyBinExpr(BinExpr("-", b, a.expr))
    case BinExpr("+", a: Node, b: Unary) => simplifyBinExpr(BinExpr("-", a, b.expr))
    case BinExpr("/", a: Node, b: Node) => simplifyDivision(simplify(a), simplify(b))

    case BinExpr(o, a: Node, b: Node) => {
      b match {
        case BinExpr("/", IntNum(1), right) => if (o == "*") return simplifyBinExpr(BinExpr("/", a, right))
        case _ =>
      }
      val simplifyA: Node = simplify(a)
      val simplifyB: Node = simplify(b)
      if (simplifyB == b && simplifyA == a) {
        if (a == b) {
          if (Set(">", "<", "!=").contains(o)) FalseConst()
          else if (Set(">=", "<=", "==").contains(o)) TrueConst()
          else if (Set("and", "or").contains(o)) a
          else if (o == "-") IntNum(0)
          else binExpr
        } else binExpr
      } else simplifyBinExpr(BinExpr(o, simplifyA, simplifyB)) //recursive
    }
  }

  def simplifyUnary(unary: Unary): Node = unary match {
    case Unary("not", Unary("not", a)) => a match {
      case unary1: Unary => simplifyUnary(unary1)
      case _ => a
    }
    case Unary("-", Unary("-", a)) => a match {
      case unary1: Unary => simplifyUnary(unary1)
      case _ => a
    }
    case Unary("not", a: LogicalConst) => logicalToConst(a.value.unary_!)
    case Unary(o, binExpr: BinExpr) => BinExpr(compareOps(binExpr.op), binExpr.left, binExpr.right)
    case _ => unary
  }

  def simplifyAssignment(assignment: Assignment): Node = {
    val simplyfied = Assignment(simplify(assignment.left), simplify(assignment.right))
    if (simplyfied.left != simplyfied.right) {
      simplyfied match {
        //todo
        case _ => simplyfied
      }
    }
    else EmptyNode()
  }

  def simplifyKeyDatumList(keyDatumList: KeyDatumList): Node = {
    var list = List[KeyDatum]()
    keyDatumList.list.toStream.foreach(kd => if (!list.toStream.map(kd2 => kd2.key).contains(kd.key)) {
      list = list.++(keyDatumList.list.reverse.find(kd2 => kd2.key == kd.key))
    })
    KeyDatumList(list)
  }

  def simplifyWhileInstr(whileInstruction: WhileInstr): Node = whileInstruction match {
    case WhileInstr(cond: FalseConst, body: Node) => EmptyNode()
    case _ => whileInstruction
  }

  def simplifyIfElseExpr(ifElse: IfElseExpr): Node = ifElse match {
    case IfElseExpr(cond: LogicalConst, a: Node, b: Node) => if (cond.isInstanceOf[TrueConst]) a else b
    case _ => ifElse
  }

  def simplifyIfElseInstr(ifElseInstr: IfElseInstr): Node = ifElseInstr match {
    case IfElseInstr(cond: LogicalConst, a: Node, b: Node) => if (cond.isInstanceOf[TrueConst]) simplify(a) else simplify(b)
    case _ => ifElseInstr
  }

  def removeDeadAssignments(nodes: List[Node]): List[Node] = {
    if (nodes.isEmpty || nodes.length == 1 ) return nodes
    val retList: List[Node] = List(nodes.last)
    for (i <- 1 until nodes.length){
      (retList.last, nodes.reverse(i)) match {
        case (Assignment(l1, r1), Assignment(l2, r2)) if l1 == l2 =>
        case _ => retList.++(List(nodes.reverse(i)))
      }
    }
    retList.reverse
  }

  def simplify(node: Node): Node = node match {
    case l: NodeList if l.list.length == 1 => {
      val node = simplify(l.list.head)
      if (node.isInstanceOf[EmptyNode]) simplify(NodeList(List())) else node
    }
    case l: NodeList => {
      val nodeList: NodeList = NodeList(removeDeadAssignments(l.list.map(simplify).filter(node => !node.isInstanceOf[EmptyNode]).filter(_ != NodeList(List()))))
      if (nodeList.list.length == 1) nodeList.list.head else nodeList
    }
    case binExpr: BinExpr => simplifyBinExpr(binExpr)
    case unary: Unary => simplifyUnary(Unary(unary.op, simplify(unary.expr)))
    case assignment: Assignment => simplifyAssignment(assignment)
    case keyDatumList: KeyDatumList => simplifyKeyDatumList(keyDatumList)
    case whileInstruction: WhileInstr => simplifyWhileInstr(whileInstruction)
    case ifElse: IfElseExpr => simplifyIfElseExpr(ifElse)
    case ifElseInstr: IfElseInstr => simplifyIfElseInstr(ifElseInstr)
    case _ => node
  }


}
